> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advanced Mobile Applications Development

## Brian Kelly

### LIS 4368 Requirements:

*Course Work Links (Assignments):*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK and Tomcat 
    - Installation Screenshots
    - Create bitbucket repo 
    - Complete bitbucket tutorials 
    - Provide git command descriptions 
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Screenshot of querybook.html
    - Screenshot of query results
    - Screenshot of A2 Index.jsp
    - Skillsets 1-3
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshot of ERD
    - Screenshot of 10 records for each table
    - Screenshot of A3 Index.jsp
    - SQL links
    - Skillsets 4-6
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - 
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - 


*Course Work Links (Projects):*

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Screenshot of data fields being required
    - Screenshot of data fields with data in it
    - Screenshot of Skillset 7-9
2. [P2 README.md](p2/README.md "My P2 README.md file")
    -  




