

# LIS 4368

## Brian Kelly

### Assignment #1  Requirements:

*Completed Steps:*

1. Screenshot of running java Hello
2. Screenshot of running http://localhost:9999
3. Screenshot of a1/index.jsp 
4. git commands w/short descriptions
5. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of running AMPPS
* Screenshot of running Android Studio My First App
* Screenshot of running Android Studio Contacts App
* Git commands with short descriptions
* Bitbucket repo links

> #### Git commands w/ short descriptions:

1. git-init - Create an empty Git repository or reinitialize an existing one
2. git-status - Show the working tree status
3. git-add - Add file contents to the index
4. git-commit - Record changes to the repository
5. git-push - Update remote refs along with associated objects
6. git-pull - Fetch from and integrate with another repository or a local branch
7. git-config - Get and set repository or global options

#### Assignment Screenshots:

| *Screenshot of running AMPPS*                | *Screenshot of Java Hello*|
| :---                                         | :---                      |
|![AMPPS Installation Screenshot](img/ampps.png)|![Java Hello Program Screenshot](img/javaHello.PNG)|

| *Screenshot of Tomcat Installation* |
| :---                                |
|![Tomcat Installation Screenshot](img/tomcatScreenshot.png)|

|*GIF of A1 index.jsp Running*|
|:---                               |
|![A1 index.jsp GIF](img/a1index.gif)|



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bpk16/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

