


# LIS4368 Advanced Web Applications Development

## Brian Kelly

### Assignment #2 Requirements:

1. Screenshot of querybook.html
2. Screenshot of query results
3. Screenshot of A2 Index.jsp
4. Screenshot of skillset 1
5. Screenshot of skillset 2
6. screenshot of skillset 3

#### Assignment Links:

- [http://localhost:9999/hello](http://localhost:9999/hello)
- [http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html)
- [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)
- [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
- [http://localhost:9999/lis4368](http://localhost:9999/hello/lis4368)

#### Assignment Screenshots:

|Gif of A2 Index.jsp*|
| :---               |
|![A2 Index.jsp](img/a2IndexJSP.gif)|

|*Screenshot of Querybook.html*|*Screenshot of Query Results (Ah Teck)*|
| :---                                         | :---                  |
|![Querybook.html](img/querybookpic.png)|![Query Results (Ah Teck)](img/ahTeckQueryPic.png)|

|*Screenshot of Query Results (Ali)*|*Screenshot of Query Results (Kumar)*|
| :---                              | :---                                |
|![Query Results (Ali)](img/AliQueryPic.png)|![Query Results (Kumar)](img/KumarQueryPic.png)|

|*Screenshot of Skillset 1*|
| :---                     |
|![Skillset 1 Screenshot](img/lis4368Skillset1.png)|

|*Screenshot of Skillset 2*|*Screenshot of Skillset 3*|   
| :---                     | :---                     |
|![Skillset 2 Screenshot](img/lis4368Skillset2.png)|![Skillset 3 Screenshot](img/lis4368Skillset3.png)|