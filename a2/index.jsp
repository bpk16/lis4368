<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 19:38:14 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="This is my online portfolio that displays the skills I have acquired while taking courses at Florida State University.">
	<meta name="author" content="Brian Kelly">
	<link rel="icon" href="img/favicon.ico">

	<title>LIS4368 - Assignment2</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<body style = background-color:grey>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<b>Servlet Compilation and Usage:</b><br />
					<img src="img/sayHelloSerlevtPic.PNG" class="img-responsive center-block" alt="Using Servlets" />

					<br /> <br />
					<b>Database Connectivity Using Servlets:</b><br />
					<img src="img/querybookpic.png" class="img-responsive center-block" alt="Database Connectivity Using Servlets" />
				<br />
					<img src="img/ahTeckQueryPic.png" class="img-responsive center-block" alt="Database Connectivity Using Servlets" />
				<br />
					<img src="img/AliQueryPic.png" class="img-responsive center-block" alt="Database Connectivity Using Servlets" />
				<br />
					<img src="img/KumarQueryPic.png" class="img-responsive center-block" alt="Database Connectivity Using Servlets" />

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>		

</body>
</html>
