


# LIS4368 Advanced Web Applications Development

## Brian Kelly

### Assignment #3 Requirements:

1. Screenshot of ERD
2. Screenshot of 10 records for each table
3. Screenshot of A3 Index.jsp
4. SQL links
5. Skillsets 4-6


#### Assignment Links

|*A3 docs: a3.mwb and a3.sql*| 
| :---                       |
|[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")|
|[A3 SQL File](docs/a3.sql "A3 SQL Script")|


#### Assignment Screenshots:

|*Screenshot A3 ERD*|
| :---              |
|![A3 ERD](img/a3.PNG)|

|*Screenshot Petstore Records*|*Screenshot Petstore Records*|*Screenshot Pet Records*|
| :---                        | :---                        | :---                   |
|![Petstore Records](img/petStoreRecords.png)|![Customer Records](img/customerRecords.png)|![Pet Records](img/petRecords.png)|

|*Screenshot A3 Index.jsp* |
| :---                     | 
|![A3 Index.jsp](img/a3Index.png)|

|*Screenshots of Skillset 4*|
| :---                      |
|![Skillset 4 Screenshots](img/skillSet4.PNG)|

|*Screenshots of Skillset 5*|
| :---                      |
|![Skillset 5 Screenshots](img/skillSet5.PNG)|

|*Screenshots of Skillset 6*|
| :---                      |
|![Skillset 6 Screenshots](img/skillSet6.PNG)|