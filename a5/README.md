
# LIS4331 Advanced Mobile Applications Development

## Brian Kelly

### Assignment #5 Requirements:

1. Screenshot of News Reader App Items Activity Screen
2. Screenshot of News Reader App Item Activity Screen
3. Screenshot of News Reader App Read More Screen
4. Screenshot of Skillset 13
5. Screenshot of Skillset 14
6. Screenshot of Skillset 15

#### Assignment Requirements:

|*Screenshot of News Reader App Items Activity*|*Screenshot of News Reader App Item Activity*|*Screenshot of News Reader App Read More*|
|:---                               |:---                               |:---                               |
|[![News Reader App Items Activity](img/newsReaderAppItemsActivity.png)]|[![News Reader App Item Activity](img/newsReaderAppItemActivity.png)]|[![News Reader App Read More](img/newsReaderAppItemActivity.png)]|

|*Skillset 13 Screenshot*|*Skillset 14 Screenshot*|
| :---                        | :---                        |
|![Skillset 13 Screenshot](img/skillSet13.PNG)|![Skillset 14 Screenshot](img/skillSet14.PNG)|

|*Skillset 15 Screenshot*|
| :---                        |
|![Skillset 15 Screenshot](img/skillSet15.PNG)|