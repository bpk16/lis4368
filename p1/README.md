
# LIS4368 Advanced Web Applications Development

## Brian Kelly

### Project #1 Requirements:

1. Screenshot of data fields being required
2. Screenshot of data fields with data in it
3. Screenshot of Skillset 7-9

#### Assignment Requirements:

|*Screenshot of data fields being required*|
|:---                                      |
|![Data fields being required](img/dataFieldsRequiresPic.png)|

|*Screenshot of data fields with data in it*|
|:---                                      |
|![Data fields with data in it](img/dataFieldsInputPic.png)|

|*Skillset 7 Screenshot*|
| :---                  |
|![Skillset 7 Screenshot](img/SkillSet7.png)|

|*Skillset 8 Screenshot*|
| :---                  |
|![Skillset 8 Screenshot](img/SkillSet8.png)|

|*Skillset 9 Screenshot*|
| :---                  |
|![Skillset 9 Screenshot](img/SkillSet9.png)|