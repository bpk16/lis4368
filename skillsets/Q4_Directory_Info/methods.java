import java.io.File;
import java.util.Scanner;

public class methods 
{
    public static void getRequirements()
    {
        System.out.println("Developer: Brian Kelly");
        System.out.println("Program lists files and subdirectories of user-specified directory.\n");

        System.out.println();
    }

    public static void directoryInfo()
    {
        String myDir = "";
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter drectory path: ");
        myDir = sc.nextLine();

        File directoryPath = new File(myDir);

        File filesList[] = directoryPath.listFiles();

        System.out.println("List files and directories in specified directory:");
        for(File file : filesList)
        {
            System.out.println("Name: " + file.getName());
            System.out.println("Path: " + file.getAbsolutePath());
            System.out.println("Size (Bytes): " + file.length());
            System.out.println("Size (KB): " + file.length()/(1024));
            System.out.println("Size (MB): " + file.length()/(1024 * 1024));
            System.out.println();
        }

        System.out.println("----------------------------------------------------");

        try
        {
            File[] files = directoryPath.listFiles();

            for(int i =0; i < files.length;i++)
        {
            System.out.println("Name: " + files[i].getName());
            System.out.println("Path: " + files[i].getAbsolutePath());
            System.out.println("Size (Bytes): " + files[i].length());
            System.out.println("Size (KB): " + files[i].length()/(1024));
            System.out.println("Size (MB): " + files[i].length()/(1024 * 1024));
            System.out.println();
        }
    }
    
    catch(Exception e)
    {
        System.err.println(e.getMessage());
    }
    sc.close();
    }
}