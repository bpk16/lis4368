import java.util.Scanner;

public class methods 
{
    public static void getRequirements()
    {
        System.out.println("Developer: Brian Kelly");
        System.out.println("Program determines total number of characters in line of text,");
        System.out.println("as well as number of times specific character is used.");
        System.out.println("Program displays chcaracter's SCII value.");

        System.out.println();

        System.out.println("References:");
        System.out.println("ASCII Background: https://en.wikipedia.org/wiki/ASCII");
        System.out.println("ASCII Character Table: https://www.ascii-code.com/");
        System.out.println("Lookup Tables: https://www.lookuptables.com/");

        System.out.println();
    }
    public static void characterInfo()
    {
        String str = "";
        char ch = ' ';
        int len = 0;
        int num = 0;
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Please enter line of text: ");
        str = sc.nextLine();
        len = str.length();

        System.out.print("Please enter character to check: ");
        ch = sc.next().charAt(0);

        for(int i = 0; i < len;i++)
        {
            if (ch == str.charAt(i))
            {
                ++num;
            }
        }
        System.out.println("\nNumber of characters in line of text: " + len);
        System.out.println("The character " + ch + " appears " + num + " time(s) in line of text.");
        System.out.println("ASCII value of " + ch + ": " + (int)ch);
        sc.close();
    }
}