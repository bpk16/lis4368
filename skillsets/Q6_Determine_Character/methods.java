import java.util.Scanner;

import javax.lang.model.util.ElementScanner14;

public class methods 
{
    public static void getRequirements()
    {
        System.out.println("Developer: Brian Kelly");
        System.out.println("Program determines whether user-entered value is a vowel, consonant, special character, or integer");
        System.out.println("Program displays chcaracter's SCII value.");

        System.out.println();

        System.out.println("References:");
        System.out.println("ASCII Background: https://en.wikipedia.org/wiki/ASCII");
        System.out.println("ASCII Character Table: https://www.ascii-code.com/");
        System.out.println("Lookup Tables: https://www.lookuptables.com/");

        System.out.println();
    }
    public static void determineChar()
    {
        char ch = ' ';
        char chTest = ' ';
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter a character: ");
        ch = sc.next().charAt(0);
        chTest = Character.toLowerCase(ch);

        if(chTest == 'a' || chTest == 'e' ||chTest == 'i' || chTest == 'o' || chTest == 'u' || chTest == 'y')
        {
            System.out.println(ch + " is a vowel. ASCII value is: " + (int)ch);
        }
        else if(ch >= '0' && ch <= '9')
        {
            System.out.println(ch + " is an integer. ASCII value is: " + (int)ch);
        }
        else if((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
        {
            System.out.println(ch + " is a consonant. ASCII value is: " + (int)ch);
        }
        else 
        {
            System.out.println(ch + " is a special character. ASSCII value is: " + (int)ch);
        }
        sc.close();
    }
}
